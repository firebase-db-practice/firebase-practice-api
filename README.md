# Firebase Database - API Practice

[![pipeline status](https://gitlab.com/firebase-practice-velantcode/firebase-practice-api/badges/master/pipeline.svg)](https://gitlab.com/firebase-practice-velantcode/firebase-practice-api/commits/master)

This is a simple practice to use `Firebase Database` with Node.js and Express. You can download this repo and practice.


## Step required

* Clone this repository: `git clone git@gitlab.com:firebase-practice-velantcode/firebase-practice-api.git`

* Create a new project in Firebase. [GO TO CONSOLE](https://console.firebase.google.com/)

* Create a new `Firebase Database`. Once created, copy the Database URL to add in the `.env` file.

* Go to the `Project Settings > Service Accounts` and generate a new `PRIVATE_KEY` and download the `.json` file to use in this project.

* Configure a `.env` file from `.env.example` file.
    
    
    # Indicates a port
    API_PORT=3000
    
    # Add the FIREBASE_DATABASE_URL
    FIREBASE_DATABASE_URL="https://your-url-database.firebase.app/"
    
    # Indicates the path (absolute) of PRIVATE_KEY.json file.
    FIREBASE_KEY_FILENAME="path/to/PRIVATE_KEY.json"
    
    # If you want to generate the logs file, change the value to: true
    LOGS_FILE=true
    

## Configures and run

``` bash
# Install dependencies
$ npm i

# Serve with hot reload at localhost:3000 to dev
$ npm run dev

# Server without hot reload
$ npm run start

# Confirms that the server works
http://localhost:9000/

# Generate documentation API
$ npm run docs

# Access to documentation in the broswer
http://localhost:3000/apidoc

# If you want build and run a server, execute:
$ npm run build
$ npm run serve
```

## More documentation 

#### Express
Official documentation: [Express.js docs](https://expressjs.com).

#### Firebase JavaScript SDK Reference For Node.js
Official documentation: [Firebase SDK to Node.js docs](https://firebase.google.com/docs/reference/node?authuser=0).
