import { v4 as uuidv4 } from 'uuid';
import DB from '../database';

const pathFile = 'src/Controllers/Contacts.js';

class ContactsController {
  async get(req, res) {
    try {
      // get contacts once
      const list = [];
      await DB.ref('contacts').once('value', (snapshot) => {
        snapshot.forEach((childSnapshot) => {
          const key = childSnapshot.key;
          const data = childSnapshot.val();

          // push data with the key contact
          list.push(Object.assign(data, { key }));
        });
      });

      return res.status(200).json({
        msg: 'Contacts',
        length: list.length,
        contacts: list,
      });
    } catch (e) {
      console.error(`Error: ${pathFile}/get`, e.toString());
      return res.status(500).json({
        msg: 'Error server.',
        errors: [
          {
            msg: e.toString(),
          },
        ],
      });
    }
  }

  async details(req, res) {
    try {
      // get contacts once
      let ret = null;
      const { key } = req.params;

      await DB.ref(`contacts/${key}`).once('value', (snapshot) => {
        if (snapshot.exists()) {
          ret = Object.assign(snapshot.val(), { _id: snapshot.key });
        }
      });

      if (ret) {
        return res.status(200).json({
          msg: 'Contact',
          contact: ret,
        });
      }
      return res.status(404).json({
        msg: 'Contact',
        contact: null,
      });
    } catch (e) {
      console.error(`Error: ${pathFile}/details`, e.toString());
      return res.status(500).json({
        msg: 'Error server.',
        errors: [
          {
            msg: e.toString(),
          },
        ],
      });
    }
  }

  async save(req, res) {
    try {
      // validate data
      const validation = ContactsController.validations(req.body);

      // check if exist errors
      if (validation.errors.length > 0) {
        return res.status(422).json({
          msg: 'Unprocessable Entity',
          errors: validation.errors,
        });
      }

      // check if exist the phone number
      let exist = await ContactsController.checkExist(validation.data.phone);

      if (exist) {
        const key = uuidv4();
        await DB.ref(`contacts/${key}`).set(validation.data);

        return res.status(200).json({
          msg: 'The contact was successful added.',
          added: true,
          key,
        });
      }

      return res.status(422).json({
        msg: 'The phone number was added earlier.',
        added: false,
        exist: true,
      });
    } catch (e) {
      console.error(`Error: ${pathFile}/save`, e.toString());
      return res.status(500).json({
        msg: 'Error server.',
        errors: [
          {
            msg: e.toString(),
          },
        ],
      });
    }
  }

  async update(req, res) {
    try {
      const { key } = req.params;

      // validate data
      const validation = ContactsController.validations(req.body);

      if (validation.errors.length > 0) {
        return res.status(422).json({
          msg: 'Unprocessable Entity',
          errors: validation.errors,
        });
      }

      let update = false;

      // check if exist data contact
      await DB.ref(`contacts/${key}`).once('value', async (snapshot) => {
        if (snapshot.exists()) {
          update = true;
        }
      });

      if (update) {
        // check if the phone number exist in another contact
        if (await ContactsController.checkExist(validation.data.phone, key)) {
          await DB.ref(`contacts/${key}`).update(validation.data);

          return res.status(422).json({
            msg: 'Contact was successful updated.',
            updated: true,
          });
        }

        return res.status(422).json({
          msg: 'The phone number already exists.',
          updated: false,
          exist: true,
        });
      }

      return res.status(422).json({
        msg: 'The contact not found.',
        updated: false,
        exist: false,
      });
    } catch (e) {
      console.error(`Error: ${pathFile}/update`, e.toString());
      return res.status(500).json({
        msg: 'Error server.',
        errors: [
          {
            msg: e.toString(),
          },
        ],
      });
    }
  }

  async delete(req, res) {
    try {
      const { key } = req.params;

      const contact = await DB.ref(`contacts/${key}`).once('value');

      if (contact.exists()) {
        // Delete contact
        DB.ref(`contacts/${key}`).remove();

        return res.status(200).json({
          msg: 'Contact was successful deleted.',
          deleted: true,
        });
      }

      return res.status(404).json({
        msg: 'Contact not found.',
      });
    } catch (e) {
      console.error(`Error: ${pathFile}/delete`, e.toString());
      return res.status(500).json({
        msg: 'Error server.',
        errors: [
          {
            msg: e.toString(),
          },
        ],
      });
    }
  }

  // ======================================================================================================

  /**
   * @function Validation data
   * @param {Object} data Contact data.
   * @return {Object} ret Return the contact data and error list if exist.
   */
  static validations(data) {
    const ret = {
      data: {
        firstName: null,
        lastName: null,
        email: undefined,
        phone: null,
      },
      errors: [],
    };

    if (
      !data.firstName ||
      data.firstName.length === 0 ||
      !/^([A-Z\u00C0-\u024F\u1E00-\u1EFF]{1})+([[A-Za-z\u00C0-\u024F\u1E00-\u1EFF]+[,.]?[ ]?|[A-Za-z\u00C0-\u024F\u1E00-\u1EFF]+['-]]?)+$/.test(
        data.firstName
      )
    ) {
      ret.errors.push({
        msg:
          "You must indicate the contact's name. Only allow letters (A-Z, a-z) and whitespaces.",
        input: 'firstName',
      });
    } else {
      ret.data.firstName = data.firstName.trim();
    }

    if (
      !data.lastName ||
      data.lastName.length === 0 ||
      !/^([A-Z\u00C0-\u024F\u1E00-\u1EFF]{1})+([[A-Za-z\u00C0-\u024F\u1E00-\u1EFF]+[,.]?[ ]?|[A-Za-z\u00C0-\u024F\u1E00-\u1EFF]+['-]]?)+$/.test(
        data.lastName
      )
    ) {
      ret.errors.push({
        msg:
          "You must indicate the contact's last name. Only allow letters (A-Z, a-z) and whitespaces.",
        input: 'lastName',
      });
    } else {
      ret.data.lastName = data.lastName.trim();
    }

    // if exist email, check.
    if (data.email) {
      if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(data.email)) {
        ret.errors.push({
          msg:
            'You must indicate a valid email address. Example: user@example.com',
          input: 'email',
        });
      } else {
        ret.data.email = data.email.toLocaleLowerCase().trim();
      }
    } else {
      ret.data.email = null;
    }

    if (
      !data.phone ||
      !/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(
        data.phone
      )
    ) {
      ret.errors.push({
        msg:
          "You must indicate the contact's phone number. Only allow numbers (0-9) Example: 04121234567 or 1234567.",
        input: 'phone',
      });
    } else {
      ret.data.phone = data.phone.trim();
    }

    return ret;
  }

  static async checkExist(phone, keyIgnore) {
    let exist = 0;
    const contacts = await DB.ref(`contacts`).once('value');
    await contacts.forEach((doc) => {
      const d = doc.val();
      // in case to change the phone number of a contact exist
      if (keyIgnore && doc.key !== keyIgnore && d.phone && d.phone === phone) {
        exist++;
      }
      // in case is a new contact phone number
      else if (!keyIgnore && d.phone && d.phone === phone) {
        exist++;
      }
    });

    return exist === 0;
  }
}

module.exports = new ContactsController();
