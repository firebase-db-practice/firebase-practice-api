// prettier-ignore
/**
 * @api {post} /contacts Create a new contact
 * @apiVersion 1.0.0
 * @apiName contactCreate
 * @apiGroup Contacts
 *
 * @apiParam {String} firstName Contact's names.
 * @apiParam {String} lastName Contact's last names.
 * @apiParam {String} phone Contact's phone number.
 * @apiParam {String} email[email] Contact's email address (optional).
 *
 * @apiExample {JSON} Example JSON Request
 *{
	"firstName": "Contact",
	"lastName": "Test",
	"phone": "04141234567",
	"email": "contact@example.com"
}
 *
 * @apiSuccess {String} msg Process message.
 * @apiSuccess {Boolean} added Value if contact was added.
 * @apiSuccess {String} key Contact's key.
 *
 * @apiSuccessExample {JSON} Success
 * HTTP/1.1 200 Success
 * {
    "msg": "The contact was successful added.",
    "added": true,
    "_id": "c8b00cd3-b448-422a-a741-9afdf1cde211"
}
 *
 * @apiError {String} msg General error message.
 * @apiError {Array} errors[errors] Error messages list.
 * @apiError (errors object array) {String} msg Error information.
 * @apiError (errors object array) {String} input[input] Input in validation case (optional).
 *
 * @apiErrorExample {JSON} Validation fields
 * HTTP/1.1 422 Unprocessable Entity
 * {
    "msg": "Unprocessable Entity",
    "errors": [
      {
        "msg": "You must indicate the contact's name. Only allow letters (A-Z, a-z) and whitespaces.",
        "input": "firstname"
      }
    ]
  }
 *
 * @apiErrorExample {JSON} Error internal server
 * HTTP/1.1 500 Internal Error Server
 * {
    "msg": "Error internal server.",
    "errors": [${err}],
  }
 */

/**
 * @api {get} /contacts Get contact list
 * @apiVersion 1.0.0
 * @apiName contactList
 * @apiGroup Contacts
 *
 * @apiSuccess {String} msg Process message.
 * @apiSuccess {Number} length Total contacts obtained.
 * @apiSuccess {Array} contacts Contact list obtained.
 *
 * @apiSuccess (contacts array object) {String} firstName Contact's names.
 * @apiSuccess (contacts array object) {String} lastName Contact's last names.
 * @apiSuccess (contacts array object) {String} phone Contact's phone number.
 * @apiSuccess (contacts array object) {String} email Contact's email address.
 *
 * @apiSuccessExample {JSON} Success with data
 * HTTP/1.1 200 Success
 * {
    "msg": "Contacts",
    "length": 2,
    "contacts": [
        {
            "email": "test1@example.com",
            "firstName": "Test One",
            "lastName": "Contact",
            "phone": "04121234567",
            "key": "b280c600-0540-42d1-90c8-c678d8b49d41"
        },
        {
            "email": "test2@example.com",
            "firstName": "Test Two",
            "lastName": "Contact",
            "phone": "+584141234567",
            "key": "dc97a49e-9d82-4956-a212-8fdb60ba02b9"
        }
    ]
}
 *
 * @apiSuccessExample {JSON} Success without data
 * HTTP/1.1 200 Success
 * {
    "msg": "Contacts",
    "length": 0,
    "contacts": []
}
 *
 * @apiError {String} msg General error message.
 * @apiError {Array} errors[errors] Error messages list.
 * @apiError (errors object array) {String} msg Error information.
 *
 * @apiErrorExample {JSON} Error internal server
 * HTTP/1.1 500 Internal Error Server
 * {
    "msg": "Error internal server.",
    "errors": [${err}],
  }
 */

/**
 * @api {get} /contacts/:key Get contact details
 * @apiVersion 1.0.0
 * @apiName detailsContact
 * @apiGroup Contacts
 *
 * @apiParam (Path param) {String} key Contact's key.
 *
 * @apiSuccess {String} msg Process message.
 * @apiSuccess {Object} contact Contact data.
 *
 * @apiSuccess (contact object) {String} firstName Contact's names.
 * @apiSuccess (contact object) {String} lastName Contact's last names.
 * @apiSuccess (contact object) {String} phone Contact's phone number.
 * @apiSuccess (contact object) {String} email Contact's email address.
 *
 * @apiSuccessExample {JSON} Success
 * HTTP/1.1 200 Success
 * {
    "msg": "Contact",
    "contact": {
        "email": "eukaris7794@gmail.com",
        "firstName": "Eukaris Josefina",
        "lastName": "Espinoza",
        "phone": "04120848775",
        "_id": "b280c600-0540-42d1-90c8-c678d8b49d41"
    }
}
 *
 * @apiError {String} msg General message.
 * @apiError {Array} errors[errors] Error messages list.
 * @apiError (errors object array) {String} msg Error information.
 *
 * @apiErrorExample {JSON} Not found
 * HTTP/1.1 404 Not found
 * {
    "msg": "Contact",
    "contact": null
}
 *
 * @apiErrorExample {JSON} Error internal server
 * HTTP/1.1 500 Internal Error Server
 * {
    "msg": "Ha ocurrido un error inesperado.",
    "errors": [${err}],
  }
 */

/**
 * @api {put} /contact/:key Update contact data
 * @apiVersion 1.0.0
 * @apiName updateContact
 * @apiGroup Contacts
 *
 * @apiParam (Path param) {String} key Contact's key.
 *
 * @apiParam {String} firstName Contact's names.
 * @apiParam {String} lastName Contact's last names.
 * @apiParam {String} phone Contact's phone number.
 * @apiParam {String} email[email] Contact's email address (optional).
 *
 * @apiExample {JSON} Example JSON Request
 *{
	"firstName": "Test Two",
	"lastName": "Contact",
	"phone": "04141234567",
	"email": "contact@example.com"
}
 *
 * @apiSuccess {String} msg Process message.
 * @apiSuccess {Boolean} updated Value if contact was updated.
 *
 * @apiSuccessExample {JSON} Success
 * HTTP/1.1 200 Success
 * {
    "msg": "Contact successful updated.",
    "updated": true
}
 *
 * @apiError {String} msg General error message.
 * @apiError {Array} errors[errors] Error messages list.
 * @apiError (errors object array) {String} msg Error information.
 * @apiError (errors object array) {String} input[input] Input in validation case (optional).
 *
 * @apiErrorExample {JSON} Validation fields
 * HTTP/1.1 422 Unprocessable Entity
 * {
    "msg": "Unprocessable Entity",
    "errors": [
      {
        "msg": "You must indicate the contact's name. Only allow letters (A-Z, a-z) and whitespaces.",
        "input": "firstname"
      }
    ]
  }
 *
 * @apiError {String} msg General message.
 * @apiErrorExample {JSON} Error internal server
 * HTTP/1.1 500 Internal Error Server
 * {
    "msg": "Error internal server.",
    "errors": [${err}],
  }
 */

// prettier-ignore
/**
 * @api {post} /contacts Create a new contact
 * @apiVersion 1.0.0
 * @apiName contactCreate
 * @apiGroup Contacts
 *
 * @apiParam {String} firstName Contact's names.
 * @apiParam {String} lastName Contact's last names.
 * @apiParam {String} phone Contact's phone number.
 * @apiParam {String} email[email] Contact's email address (optional).
 *
 * @apiExample {JSON} Example JSON Request
 *{
	"firstName": "Contact",
	"lastName": "Test",
	"phone": "04141234567",
	"email": "contact@example.com"
}
 *
 * @apiSuccess {String} msg Process message.
 * @apiSuccess {Boolean} added Value if contact was added.
 * @apiSuccess {String} key Contact's key.
 *
 * @apiSuccessExample {JSON} Success
 * HTTP/1.1 200 Success
 * {
    "msg": "The contact was successful added.",
    "added": true,
    "_id": "c8b00cd3-b448-422a-a741-9afdf1cde211"
}
 *
 * @apiError {String} msg General error message.
 * @apiError {Array} errors[errors] Error messages list.
 * @apiError (errors object array) {String} msg Error information.
 * @apiError (errors object array) {String} input[input] Input in validation case (optional).
 *
 * @apiErrorExample {JSON} Validation fields
 * HTTP/1.1 422 Unprocessable Entity
 * {
    "msg": "Unprocessable Entity",
    "errors": [
      {
        "msg": "You must indicate the contact's name. Only allow letters (A-Z, a-z) and whitespaces.",
        "input": "firstname"
      }
    ]
  }
 *
 * @apiErrorExample {JSON} Error internal server
 * HTTP/1.1 500 Internal Error Server
 * {
    "msg": "Error internal server.",
    "errors": [${err}],
  }
 */

/**
 * @api {get} /contacts Get contact list
 * @apiVersion 1.0.0
 * @apiName contactList
 * @apiGroup Contacts
 *
 * @apiSuccess {String} msg Process message.
 * @apiSuccess {Number} length Total contacts obtained.
 * @apiSuccess {Array} contacts Contact list obtained.
 *
 * @apiSuccess (contacts array object) {String} firstName Contact's names.
 * @apiSuccess (contacts array object) {String} lastName Contact's last names.
 * @apiSuccess (contacts array object) {String} phone Contact's phone number.
 * @apiSuccess (contacts array object) {String} email Contact's email address.
 *
 * @apiSuccessExample {JSON} Success with data
 * HTTP/1.1 200 Success
 * {
    "msg": "Contacts",
    "length": 2,
    "contacts": [
        {
            "email": "test1@example.com",
            "firstName": "Test One",
            "lastName": "Contact",
            "phone": "04121234567",
            "key": "b280c600-0540-42d1-90c8-c678d8b49d41"
        },
        {
            "email": "test2@example.com",
            "firstName": "Test Two",
            "lastName": "Contact",
            "phone": "+584141234567",
            "key": "dc97a49e-9d82-4956-a212-8fdb60ba02b9"
        }
    ]
}
 *
 * @apiSuccessExample {JSON} Success without data
 * HTTP/1.1 200 Success
 * {
    "msg": "Contacts",
    "length": 0,
    "contacts": []
}
 *
 * @apiError {String} msg General error message.
 * @apiError {Array} errors[errors] Error messages list.
 * @apiError (errors object array) {String} msg Error information.
 *
 * @apiErrorExample {JSON} Error internal server
 * HTTP/1.1 500 Internal Error Server
 * {
    "msg": "Error internal server.",
    "errors": [${err}],
  }
 */

/**
 * @api {get} /contacts/:key Get contact details
 * @apiVersion 1.0.0
 * @apiName detailsContact
 * @apiGroup Contacts
 *
 * @apiParam (Path param) {String} key Contact's key.
 *
 * @apiSuccess {String} msg Process message.
 * @apiSuccess {Object} contact Contact data.
 *
 * @apiSuccess (contact object) {String} firstName Contact's names.
 * @apiSuccess (contact object) {String} lastName Contact's last names.
 * @apiSuccess (contact object) {String} phone Contact's phone number.
 * @apiSuccess (contact object) {String} email Contact's email address.
 *
 * @apiSuccessExample {JSON} Success
 * HTTP/1.1 200 Success
 * {
    "msg": "Contact",
    "contact": {
        "email": "eukaris7794@gmail.com",
        "firstName": "Eukaris Josefina",
        "lastName": "Espinoza",
        "phone": "04120848775",
        "_id": "b280c600-0540-42d1-90c8-c678d8b49d41"
    }
}
 *
 * @apiError {String} msg General message.
 * @apiError {Array} errors[errors] Error messages list.
 * @apiError (errors object array) {String} msg Error information.
 *
 * @apiErrorExample {JSON} Not found
 * HTTP/1.1 404 Not found
 * {
    "msg": "Contact",
    "contact": null
}
 *
 * @apiErrorExample {JSON} Error internal server
 * HTTP/1.1 500 Internal Error Server
 * {
    "msg": "Ha ocurrido un error inesperado.",
    "errors": [${err}],
  }
 */

/**
 * @api {put} /contact/:key Update contact data
 * @apiVersion 1.0.0
 * @apiName updateContact
 * @apiGroup Contacts
 *
 * @apiParam (Path param) {String} key Contact's key.
 *
 * @apiParam {String} firstName Contact's names.
 * @apiParam {String} lastName Contact's last names.
 * @apiParam {String} phone Contact's phone number.
 * @apiParam {String} email[email] Contact's email address (optional).
 *
 * @apiExample {JSON} Example JSON Request
 *{
	"firstName": "Test Two",
	"lastName": "Contact",
	"phone": "04141234567",
	"email": "contact@example.com"
}
 *
 * @apiSuccess {String} msg Process message.
 * @apiSuccess {Boolean} updated Value if contact was updated.
 *
 * @apiSuccessExample {JSON} Success
 * HTTP/1.1 200 Success
 * {
    "msg": "Contact successful updated.",
    "updated": true
}
 *
 * @apiError {String} msg General error message.
 * @apiError {Array} errors[errors] Error messages list.
 * @apiError (errors object array) {String} msg Error information.
 * @apiError (errors object array) {String} input[input] Input in validation case (optional).
 *
 * @apiErrorExample {JSON} Validation fields
 * HTTP/1.1 422 Unprocessable Entity
 * {
    "msg": "Unprocessable Entity",
    "errors": [
      {
        "msg": "You must indicate the contact's name. Only allow letters (A-Z, a-z) and whitespaces.",
        "input": "firstname"
      }
    ]
  }
 *
 * @apiError {String} msg General message.
 * @apiErrorExample {JSON} Error internal server
 * HTTP/1.1 500 Internal Error Server
 * {
    "msg": "Error internal server.",
    "errors": [${err}],
  }
 */

/**
 * @api {delete} /contact/:key Delete contact
 * @apiVersion 1.0.0
 * @apiName deleteContact
 * @apiGroup Contacts
 *
 * @apiParam (Path param) {String} key Contact's key.
 *
 * @apiSuccess {String} msg Process message.
 * @apiSuccess {Boolean} deleted Value if contact was delete.
 *
 * @apiSuccessExample {JSON} Success
 * HTTP/1.1 200 Success
 * {
    "msg": "Contact successful updated.",
    "deleted": true
}
 *
 *
 * @apiError {String} msg General error message.
 * @apiError {Array} errors[errors] Error messages list.
 * @apiError (errors object array) {String} msg Error information.
 *
 * @apiErrorExample {JSON} Not found
 * HTTP/1.1 404 Not found
 * {
    "msg": "Contact not found."
  }
 *
 * @apiErrorExample {JSON} Error internal server
 * HTTP/1.1 500 Internal Error Server
 * {
    "msg": "Error internal server.",
    "errors": [${err}],
  }
 */