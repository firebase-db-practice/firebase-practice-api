// prettier-ignore
// prettier-ignore
/**
 * @api {get} / Test server
 * @apiVersion 1.0.0
 * @apiName testServer
 * @apiGroup Test
 *
 * @apiSuccess {String} msg Process message.
 *
 * @apiSuccessExample {JSON} Success
 * HTTP/1.1 200 Success
 * {
    "msg": "Hello World!!"
}
 */