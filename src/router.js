import { Router } from 'express';
import ContactsController from './Controllers/Contacts';

const router = Router();

router.get('/', (req, res) => {
  return res.status(200).json({
    msg: 'Hello World!!',
  });
});

// =====================================================================================================================

// CONTACTS

router.get('/contacts', (req, res) => {
  return ContactsController.get(req, res);
});

router.get('/contacts/:key', (req, res) => {
  return ContactsController.details(req, res);
});

router.post('/contacts', (req, res) => {
  return ContactsController.save(req, res);
});

router.put('/contacts/:key', (req, res) => {
  return ContactsController.update(req, res);
});

router.delete('/contacts/:key', (req, res) => {
  return ContactsController.delete(req, res);
});

// =====================================================================================================================

module.exports = router;
